Handlebars.registerHelper('human_readable_time', function(timestamp) {
  var date = new Date(timestamp),
    real_month = date.getMonth() + 1,
    string_date = date.getFullYear() + '-' + real_month + '-' + date.getDate();
  return string_date;
});

Handlebars.registerHelper('short_description', function(text, max_length) {
  var portrait_mode_query = window.matchMedia('(max-width: 580px)');
  text = text || '';
  max_length = max_length || 1;
  if (portrait_mode_query.matches && max_length > 11) {
    max_length = 11;
  }
  return text.substring(0, max_length) + '...';
});